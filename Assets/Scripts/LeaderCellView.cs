﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using EnhancedUI.EnhancedScroller;

public class LeaderCellView : EnhancedScrollerCellView {
	public Text LeaderPositionText;
	public Text LeaderNameText;
	public Text LeaderScoreText;
	public int LeaderAvatarIconIndex;
	public int LeaderAvatarIconColorIndex;
	public Image LeaderIconImage;
	public List<Sprite> Avatars;
	public List<Color> colorList;

	public void SetData(ScrollerData data) {

		colorList = new List<Color> () {
			new Color32(237, 139, 0, 255),
			new Color32(132, 189, 0, 255),
			new Color32(255, 0, 0, 255),
			new Color32(0, 189, 242, 255),
			new Color32(198, 0, 126, 255)

		};

		LeaderNameText.text = data.leaderName;
		LeaderScoreText.text = data.leaderScore.ToString();
		LeaderAvatarIconIndex = Mathf.Min(data.leaderAvatarIndex, (Avatars.Count-1));
		LeaderAvatarIconColorIndex = Mathf.Min(data.leaderAvatarColorIndex, (colorList.Count-1));

	//	Debug.Log ("LeaderAvatarIconColorIndex="+LeaderAvatarIconColorIndex);
		LeaderIconImage.sprite = Avatars[LeaderAvatarIconIndex];
		LeaderIconImage.color = colorList [LeaderAvatarIconColorIndex];
		LeaderScoreText.color = colorList [LeaderAvatarIconColorIndex];
		LeaderNameText.color = colorList [LeaderAvatarIconColorIndex];
		LeaderPositionText.color = colorList [LeaderAvatarIconColorIndex];
		LeaderPositionText.text = data.leaderPosition.ToString ();
	}
}
