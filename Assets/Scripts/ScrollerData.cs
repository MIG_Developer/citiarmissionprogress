﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollerData {
	

	public string leaderName;
	public string leaderScore;
	public int leaderAvatarIndex;
	public int leaderAvatarColorIndex;
	public int leaderPosition;

}
