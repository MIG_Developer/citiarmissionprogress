﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MEC;

public class countDownTimer : MonoBehaviour {
	public float currTimer = 30f;

	public float timeLeft;
	public bool stop = true;

	private float minutes;
	private float seconds;

	public Text text;

	public void startTimer(float from){
		stop = false;
		timeLeft = from;
		Update();
		Timing.RunCoroutine(updateCoroutine(), Segment.SlowUpdate);
	}


	// Update is called once per frame

	void OnEnable(){
		timeLeft = currTimer;
		startTimer (currTimer);
	}




	public float getBonus()
	{
		stop = true;
		minutes = Mathf.Floor(timeLeft / 60);
		seconds = timeLeft % 60;
		currTimer = seconds;
		text.text = string.Format("{0:0}:{1:00}", minutes, seconds);

		return currTimer;

	}

	void Update() {
		if(stop) return;
		timeLeft -= Time.deltaTime;

		minutes = Mathf.Floor(timeLeft / 60);
		seconds = timeLeft % 60;
		currTimer = seconds;
		if(seconds > 59) seconds = 59;
		if(minutes < 0) {
			stop = true;
			minutes = 0;
			seconds = 0;
		}
		//        fraction = (timeLeft * 100) % 100;
	}

	 IEnumerator<float> updateCoroutine(){
		while(!stop){
			text.text = string.Format("{0:0}:{1:00}", minutes, seconds);
			yield return Timing.WaitForSeconds(0.2f);
		}
	}


}
