﻿using UnityEngine;
using MovementEffects;
using MovementEffects.Extensions;
using MEC;
using System.Collections.Generic;

public class tweenToTarget : MonoBehaviour {

	public GameObject myObject;
	public Transform target;
	public Transform closeTarget;
	public float timeToTake;

	public float holdOpen;

	public GameObject moles;


	[HideInInspector]
	public bool isOpen=false;





	public void popUpMole(){
		moles.gameObject.SetActive (true);
		if (isOpen == false) {

		
		
			var moveThereAndBack = myObject.gameObject.transform.MoTLocalSpaceMoveTo (timeToTake, target.localPosition);
			//moveThereAndBack += myObject.gameObject.transform.MoTWorldSpaceMoveTo(4f, myObject.gameObject.transform.position);

			for (int j = 0; j < moveThereAndBack.Effects.Length; j++) {
				moveThereAndBack.Effects [j].CalculatePercentDone = Easing.BackInOut;
				Movement.Run (moveThereAndBack);
			}
			isOpen = true;
			Timing.RunCoroutine(_KeepMoleOpen());
		}
	
	}


	IEnumerator<float> _KeepMoleOpen()
	{
		
		yield return Timing.WaitForSeconds(holdOpen+timeToTake);

		closeMolePop ();
		yield break;

	}


	public void closeMolePop(){
		if (isOpen == true) {
			var moveThereAndBack = myObject.gameObject.transform.MoTLocalSpaceMoveTo (timeToTake, closeTarget.localPosition);

			for (int j = 0; j < moveThereAndBack.Effects.Length; j++) {
				moveThereAndBack.Effects [j].CalculatePercentDone = Easing.Pow5InOut;
				Movement.Run (moveThereAndBack);
			}
			isOpen = false;
		}

	}

	public void resetMole () {
		isOpen = false;

		var moveToReset = myObject.gameObject.transform.MoTLocalSpaceMoveTo (.001f, closeTarget.localPosition);

	}



	public void popUp(){
		if (isOpen == false) {
			var moveThereAndBack = myObject.gameObject.transform.MoTLocalSpaceMoveTo (timeToTake, target.position);
			//moveThereAndBack += myObject.gameObject.transform.MoTWorldSpaceMoveTo(4f, myObject.gameObject.transform.position);

			for (int j = 0; j < moveThereAndBack.Effects.Length; j++) {
				moveThereAndBack.Effects [j].CalculatePercentDone = Easing.BackInOut;
				Movement.Run (moveThereAndBack);
			}
			isOpen = true;
		} else {
			var moveThereAndBack = myObject.gameObject.transform.MoTLocalSpaceMoveTo (timeToTake, closeTarget.position);
			//moveThereAndBack += myObject.gameObject.transform.MoTWorldSpaceMoveTo(4f, myObject.gameObject.transform.position);
			isOpen = false;
			for (int j = 0; j < moveThereAndBack.Effects.Length; j++) {
				moveThereAndBack.Effects [j].CalculatePercentDone = Easing.Pow5InOut;
				Movement.Run (moveThereAndBack);
			}

		}
	}




}