﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MovementEffects;
using MovementEffects.Extensions;
using UnityEngine.UI;

public class MoleManager : MonoBehaviour {

	public List<tweenToTarget> moleList;

	public List<GameObject> daMoles;

	public float minTime;
	public float maxTime;

	public float holdOpen;
	public ParticleSystem fX;

	public UIManager UImgr;

	public AudioClip sFX;

	private float randomTime;
	private int randomMole;
	private float timeCheck;

	private int molePoints=2;

	public bool gameOn = false;

	public AudioSource audioSource;



	public void startMole(){
		audioSource = GetComponent<AudioSource>();
		setupMole ();
		gameOn = true;
	}

	public void stopMole(){
		gameOn = false;
		for (int j = 0; j < moleList.Count; j++) {
			moleList [j].resetMole ();
			daMoles [j].SetActive (true);
		}
	}

	public void setupMole(){
		randomTime = Random.Range (minTime, maxTime);
		int moleCount = moleList.Count;
		float moleFloat = (float)(moleCount);
		randomMole = ((Random.Range(0, moleCount)));
		timeCheck = Time.time;
		moleList [randomMole].holdOpen = holdOpen;


	}


	public void checkMole(string whichMole){
		// Debug.Log ("checkmole = " + whichMole);	
		audioSource.PlayOneShot(sFX, 0.7F);
		if (whichMole == "Mole_0_Holder") {
			// Debug.Log ("daMole="+daMoles[0].gameObject.name);

			daMoles [0].SetActive (false);
			UImgr.updateScore (molePoints);

		} else {
			if (whichMole == "Mole_1_Holder") {
				// Debug.Log ("daMole="+daMoles[1].gameObject.name);
				daMoles [1].SetActive (false);
				UImgr.updateScore (molePoints);
			} else {
				if (whichMole == "Mole_2_Holder") {
					// Debug.Log ("daMole="+daMoles[2].gameObject.name);
					daMoles [2].SetActive (false);
					UImgr.updateScore (molePoints);
				} else {

					if (whichMole == "Mole_3_Holder") {
						// Debug.Log ("daMole="+daMoles[3].gameObject.name);
						daMoles [3].SetActive (false);
						UImgr.updateScore (molePoints);

					} else {

						if (whichMole == "Mole_4_Holder") {
							// Debug.Log ("daMole="+daMoles[4].gameObject.name);
							daMoles [4].SetActive (false);
							UImgr.updateScore (molePoints);
						} else {


							if (whichMole == "Mole_5_Holder") {
								// Debug.Log ("daMole="+daMoles[5].gameObject.name);
								daMoles [5].SetActive (false);
								UImgr.updateScore (molePoints);

							} else {

								if (whichMole == "Mole_6_Holder") {
									// Debug.Log ("daMole="+daMoles[6].gameObject.name);
									daMoles [6].SetActive (false);
									UImgr.updateScore (molePoints);

								} else {

									if (whichMole == "Mole_7_Holder") {

										// Debug.Log ("daMole="+daMoles[7].gameObject.name);
										daMoles [7].SetActive (false);
										UImgr.updateScore (molePoints);

									} else {

										if (whichMole == "Mole_8_Holder") {
											// Debug.Log ("daMole="+daMoles[8].gameObject.name);
											daMoles [8].SetActive (false);
											UImgr.updateScore (molePoints);

										}
									}

								}


							}

						}

					}
					


				}
			}

		}

	}



	// Update is called once per frame
	void Update () {
		if (gameOn == false) {
			return;
		}

		if (Time.time - timeCheck >= randomTime) {
			moleList [randomMole].popUpMole ();
			setupMole ();
		}
			

		if (Input.touchCount > 0 && Input.GetTouch (0).phase == TouchPhase.Began) {
			RaycastHit hit;
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			if (Physics.Raycast (ray, out hit)) {
				if (hit.collider != null) {
					// Debug.Log ("hit" + hit.collider.gameObject.name);
					fX.gameObject.transform.position = hit.point;
					fX.Play ();
					// Debug.Log ("checkingMole");
					checkMole (hit.collider.gameObject.name);
				}
			}
		}
//		} else {
//			if(Input.GetMouseButton(0)){
//
//				RaycastHit hit;
//				Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
//				if (Physics.Raycast (ray, out hit)) {
//					if (hit.collider != null) {
//						// Debug.Log ("hit" + hit.collider.gameObject.name);
//						fX.gameObject.transform.position = hit.point;
//						fX.Play ();
//						// Debug.Log ("checkingMole");
//						checkMole (hit.collider.gameObject.name);
//
//
//			}



		}





					}
		

