﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;
using UnityEngine.Events;
using UnityEngine.Video;
using UnityEngine.SceneManagement;
using System.Threading.Tasks;
using EnhancedUI.EnhancedScroller;

public class UIManager : MonoBehaviour {

	public CanvasGroup Splash_Panel;
	public GameObject Splash_Obj;

	public CanvasGroup User_Panel;
	public GameObject User_Obj;

	public CanvasGroup Avatar_Panel;
	public GameObject Avatar_Obj;

	public CanvasGroup MapIntro_Panel;
	public GameObject MapIntro_Obj;

	public CanvasGroup Map_Panel;
	public GameObject Map_Obj;

	public CanvasGroup Leader_Panel;
	public GameObject Leader_Obj;

	public CanvasGroup AR_Camera_Panel;
	public GameObject AR_Camera_Obj;

	public GameObject AR_Back_Button;

	public Text MeetingCode;
	public InputField NameText;
	public InputField EmailText;
	//public InputField ScoreText;

	public Text MapIntroNameText;
	public Text MapNameText;

	public Image AvatarSprite;
	public Image AvatarSpriteMapInfo;
	public Image AvatarSpriteMap;
	public List<Sprite> Avatars;

	public int currAvatar;

	public Image AvatarCircle;


	public List<Color> colorList;

	public int currColor;

	public Text userName;
	public Text SetAvatarText;

	public Text introMapScore;
	public Text mapScore;


	public CanvasGroup Fill_In_Blank;
	public GameObject Fill_In_Blank_Obj;

	public CanvasGroup Fill_In_Blank_1;
	public GameObject Fill_In_Blank_1_Obj;

	public CanvasGroup Fill_In_Blank_1A;
	public GameObject Fill_In_Blank_1A_Obj;

	public CanvasGroup Fill_In_Blank_1B;
	public GameObject Fill_In_Blank_1B_Obj;

	public CanvasGroup Fill_In_Blank_2;
	public GameObject Fill_In_Blank_2_Obj;

	public CanvasGroup Fill_In_Blank_2B;
	public GameObject Fill_In_Blank_2B_Obj;

	public CanvasGroup Fill_In_Blank_2A;
	public GameObject Fill_In_Blank_2A_Obj;


	public GameObject Fill_In_Blank_1A_Right;
	public GameObject Fill_In_Blank_1A_Wrong;

	public GameObject Fill_In_Blank_1B_Right;
	public GameObject Fill_In_Blank_1B_Wrong;

	public GameObject Fill_In_Blank_1_Complete;


	public GameObject Fill_In_Blank_2A_Right;
	public GameObject Fill_In_Blank_2A_Wrong;

	public GameObject Fill_In_Blank_2B_Right;
	public GameObject Fill_In_Blank_2B_Wrong;
	public GameObject Fill_In_Blank_2_Complete;



	public CanvasGroup MultipleChoice;
	public GameObject MultipleChoice_Obj;

	public CanvasGroup MultipleChoice_1;
	public GameObject MultipleChoice_1_Obj;

	public CanvasGroup MultipleChoice_2;
	public GameObject MultipleChoice_2_Obj;

	public CanvasGroup MultipleChoice_3;
	public GameObject MultipleChoice_3_Obj;

	public CanvasGroup MultipleChoice_4;
	public GameObject MultipleChoice_4_Obj;

	public CanvasGroup MultipleChoice_5;
	public GameObject MultipleChoice_5_Obj;


	public GameObject MultipleChoice_1a_Obj;
	public GameObject MultipleChoice_1b_Obj;

	public GameObject MultipleChoice_1a_Right;
	public GameObject MultipleChoice_1a_Wrong;

	public GameObject MultipleChoice_1b_Right;
	public GameObject MultipleChoice_1b_Wrong;

	public GameObject MultipleChoice_1_Complete;



	public GameObject MultipleChoice_2a_Obj;
	public GameObject MultipleChoice_2b_Obj;

	public GameObject MultipleChoice_2a_Right;
	public GameObject MultipleChoice_2a_Wrong;

	public GameObject MultipleChoice_2b_Right;
	public GameObject MultipleChoice_2b_Wrong;

	public GameObject MultipleChoice_2_Complete;




	public GameObject MultipleChoice_3a_Obj;
	public GameObject MultipleChoice_3b_Obj;


	public GameObject MultipleChoice_3a_Right;
	public GameObject MultipleChoice_3a_Wrong;

	public GameObject MultipleChoice_3b_Right;
	public GameObject MultipleChoice_3b_Wrong;

	public GameObject MultipleChoice_3_Complete;



	public GameObject MultipleChoice_4a_Obj;
	public GameObject MultipleChoice_4b_Obj;

	public GameObject MultipleChoice_4a_Right;
	public GameObject MultipleChoice_4a_Wrong;

	public GameObject MultipleChoice_4b_Right;
	public GameObject MultipleChoice_4b_Wrong;

	public GameObject MultipleChoice_4_Complete;



	public GameObject MultipleChoice_5a_Obj;
	public GameObject MultipleChoice_5b_Obj;

	public GameObject MultipleChoice_5a_Right;
	public GameObject MultipleChoice_5a_Wrong;

	public GameObject MultipleChoice_5b_Right;
	public GameObject MultipleChoice_5b_Wrong;

	public GameObject MultipleChoice_5_Complete;

	public GameObject Instructions;
	public GameObject Leaderboards;
	public Text LeaderText;

	public GameObject Settings;

	public Image AvatarCircle_Settings;
	public Image AvatarSpriteMapSettings;
	public Text settingsName;
	public Text settingsEmail;


	public VideoPlayer videoPlayer_1;
	public VideoPlayer videoPlayer_2;
	public VideoPlayer videoPlayer_3;
	public VideoPlayer videoPlayer_4;

//	public MediaPlayerCtrl videoPlayer_1;
//	public MediaPlayerCtrl videoPlayer_2;
//	public MediaPlayerCtrl videoPlayer_3;
//	public MediaPlayerCtrl videoPlayer_4;

	public ImageTargetPlayVideo video_1;
	public ImageTargetPlayVideo video_2;
	public ImageTargetPlayVideo video_3;
	public ImageTargetPlayVideo video_4;




	public int currScore;


	public bool ARmode;

	public GameObject vidCompletePop;

	public ScrollerController scrollContol;

	public GameObject basketBall;
	public Text NetPointsText;
	public Text MolesNetPointsText;
	public GameObject NothinButNetPanel;
	public GameController controlTheGame;

	public MoleManager moleMgr;
	public GameObject GetYourGoals;


	public countDownTimer MC_1a_timer;
	public countDownTimer MC_1b_timer;

	public countDownTimer MC_2a_timer;
	public countDownTimer MC_2b_timer;

	public countDownTimer MC_3a_timer;
	public countDownTimer MC_3b_timer;

	public countDownTimer MC_4a_timer;
	public countDownTimer MC_4b_timer;

	public countDownTimer MC_5a_timer;
	public countDownTimer MC_5b_timer;


	public Text MC_1a_plus_text;
	public Text MC_1b_plus_text;

	public Text MC_2a_plus_text;
	public Text MC_2b_plus_text;

	public Text MC_3a_plus_text;
	public Text MC_3b_plus_text;

	public Text MC_4a_plus_text;
	public Text MC_4b_plus_text;

	public Text MC_5a_plus_text;
	public Text MC_5b_plus_text;


	public countDownTimer FIB_1a_timer;
	public countDownTimer FIB_1b_timer;

	public countDownTimer FIB_2a_timer;
	public countDownTimer FIB_2b_timer;


	public Text FIB_1a_plus_text;
	public Text FIB_1b_plus_text;

	public Text FIB_2a_plus_text;
	public Text FIB_2b_plus_text;


	private int loggedIn;
	private Firebase.Auth.FirebaseAuth auth;
	private DatabaseReference reference;

	private Firebase.Auth.FirebaseUser newUser;

	private string currChild;


	private string currUserName;

	private GameObject curr_MC;

	private int FIB_1A;
	private int FIB_1B;
	private int FIB_2A;
	private int FIB_2B;

	private int MC_1;
	private int MC_2;
	private int MC_3;
	private int MC_4;
	private int MC_5;

	private int VID_1;
	private int VID_2;
	private int VID_3;
	private int VID_4;

	private const int MaxScores = 5;

	private bool subscribedToLeaders = false;






	// Use this for initialization


	public class User {
		public string username;
		public string email;
		public int score;
		public int colorIndex;
		public int avatarIndex;


		public int FIB_Done;
		public int FIB_1A;
		public int FIB_1B;
		public int FIB_2A;
		public int FIB_2B;

		public int MC_Done;
		public int MC_1;
		public int MC_2;
		public int MC_3;
		public int MC_4;
		public int MC_5;

		public int VID_1;
		public int VID_2;
		public int VID_3;
		public int VID_4;

		public User() {
		}

		public User(string username, string email, int score, int colorIndex, int avatarIndex, int FIB_Done, int FIB_1A, int FIB_1B, int FIB_2A, int FIB_2B, int MC_Done, int MC_1, int MC_2, int MC_3, int MC_4, int MC_5, int VID_1, int VID_2, int VID_3, int VID_4) {
			this.username = username;
			this.email = email;
			this.score = score;
			this.colorIndex = colorIndex;
			this.avatarIndex = avatarIndex;


			this.FIB_Done = FIB_Done;
			this.FIB_1A = FIB_1A;
			this.FIB_1B = FIB_1B;
			this.FIB_2A = FIB_2A;
			this.FIB_2B = FIB_2B;

			this.MC_Done = MC_Done;
			this.MC_1 = MC_1;
			this.MC_2 = MC_2;
			this.MC_3 = MC_3;
			this.MC_4 = MC_4;
			this.MC_5 = MC_5;

			this.VID_1 = VID_1;
			this.VID_2 = VID_2;
			this.VID_3 = VID_3;
			this.VID_4 = VID_4;

		}
	}
		



	// Handle initialization of the necessary firebase modules:
	void InitializeFirebase() {
		// Debug.Log("Setting up Firebase Auth");
		auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
		auth.StateChanged += AuthStateChanged;
		AuthStateChanged(this, null);


		// Debug.Log ("auth-"+(auth.CurrentUser == null));
	}

	// Track state changes of the auth object.
	void AuthStateChanged(object sender, System.EventArgs eventArgs) {

		if (auth.CurrentUser == null) { return; }


		// Debug.Log ("Auth.CurrentUser = "+(auth.CurrentUser==null)+" loggedIn="+loggedIn);
		//// Debug.Log ("auth is null = " + auth.CurrentUser.UserId);
		if (auth.CurrentUser != newUser) {
			bool signedIn = newUser != auth.CurrentUser && auth.CurrentUser != null;
			// Debug.Log ("signedIn=" + signedIn);
			if (!signedIn && newUser != null) {
				// Debug.Log ("Signed out " + newUser.UserId);
				loggedIn = 0;
				// LetsStart ();
			} else {

				if (signedIn) {
					newUser = auth.CurrentUser;
					// Debug.Log ("Signed in " + newUser.UserId);

					loggedIn = 1;
					//GoMapIntro ();

					//LetsStart ();
				} else {

					// Debug.Log ("Going diretly to map");
					//getSavedValues ();
					loggedIn = 1;
					//GoMapIntro ();
				} 
			}

		}
	}

	void OnDestroy() {
		auth.StateChanged -= AuthStateChanged;
		auth = null;
		if (subscribedToLeaders == true) {
			reference.Child ("users").OrderByChild("score").LimitToLast (100).ValueChanged -= HandleLeadersChanged; 
		}
	}
		


	 void Start () {
		//loggedIn = PlayerPrefs.GetInt ("Authorized", 0);
	//	// Debug.Log("Detecting current user ID="+newUser.UserId);


		LetsStart ();





	}

//	public void getSavedValues(){
//


	//	}

	public void StartMole(){
		moleMgr.startMole ();

	}


	public void StartPlay(){

		controlTheGame.StartPlay ();
		NothinButNetPanel.SetActive (true);

	}

	public void LetsStart() {
		// Debug.Log ("starting");
		currScore = 0;
		currChild = "";
		loggedIn = 0;
		currAvatar = 0;
		ARmode = false;



		introMapScore.text = "0 pts";
        mapScore.text = "0 pts";
        NetPointsText.text = "0 pts";
        MolesNetPointsText.text = "0 pts";

		 // reset score of games too
		FIB_1A = 0;
		FIB_1B = 0;
		FIB_2A = 0;
		FIB_2B = 0;

		MC_1 = 0;
		MC_2 = 0;
		MC_3 = 0;
		MC_4 = 0;
		MC_5 = 0;

		VID_1 = 0;
		VID_2 = 0;
		VID_3 = 0;
		VID_4 = 0;

		vidCompletePop.SetActive (false);

		MultipleChoice_1a_Right.SetActive (false);
		MultipleChoice_1a_Wrong.SetActive (false);
		MultipleChoice_1b_Right.SetActive (false);
		MultipleChoice_1b_Wrong.SetActive (false);
		MultipleChoice_1_Complete.SetActive (false);

		MultipleChoice_2a_Right.SetActive (false);
		MultipleChoice_2a_Wrong.SetActive (false);
		MultipleChoice_2b_Right.SetActive (false);
		MultipleChoice_2b_Wrong.SetActive (false);
		MultipleChoice_2_Complete.SetActive (false);

		MultipleChoice_3a_Right.SetActive (false);
		MultipleChoice_3a_Wrong.SetActive (false);
		MultipleChoice_3b_Right.SetActive (false);
		MultipleChoice_3b_Wrong.SetActive (false);
		MultipleChoice_3_Complete.SetActive (false);

		MultipleChoice_4a_Right.SetActive (false);
		MultipleChoice_4a_Wrong.SetActive (false);
		MultipleChoice_4b_Right.SetActive (false);
		MultipleChoice_4b_Wrong.SetActive (false);
		MultipleChoice_4_Complete.SetActive (false);

		MultipleChoice_5a_Right.SetActive (false);
		MultipleChoice_5a_Wrong.SetActive (false);
		MultipleChoice_5b_Right.SetActive (false);
		MultipleChoice_5b_Wrong.SetActive (false);
		MultipleChoice_5_Complete.SetActive (false);


		NameText.text = "";
		EmailText.text = "";
		MeetingCode.text = "";

		colorList = new List<Color> () {
			new Color32(237, 139, 0, 255),
			new Color32(132, 189, 0, 255),
			new Color32(255, 0, 0, 255),
			new Color32(0, 189, 242, 255),
			new Color32(198, 0, 126, 255)

		};

		User_Obj.SetActive (false);
		Splash_Obj.SetActive (true);
		Avatar_Obj.SetActive (false);
		MapIntro_Obj.SetActive (false);
		Map_Obj.SetActive (false);
		Leader_Obj.SetActive (false);
		AR_Camera_Obj.SetActive (false);
		Fill_In_Blank_Obj.SetActive (false);
		MultipleChoice_Obj.SetActive (false);

		basketBall.SetActive (false);
		GetYourGoals.SetActive (false);
		Settings.SetActive (false);
		Instructions.SetActive (false);


		currColor = 0;
		switchColor (currColor);


		SetAvatar (0);
		InitializeFirebase ();
	}

	public void ShowInstructions(){

		Instructions.SetActive (true);
	}

	public void ShowLeaders(){
		if (subscribedToLeaders == false) {
			subscribedToLeaders = true;
			reference.Child ("users").OrderByChild("score").LimitToLast (100).ValueChanged += HandleLeadersChanged;

		}
		Leaderboards.SetActive (true);
	}

	void HandleLeadersChanged(object sender, ValueChangedEventArgs args) {
		if (args.DatabaseError != null) {
			Debug.LogError (args.DatabaseError.Message);
			return;
		}

		if (args.Snapshot != null && args.Snapshot.ChildrenCount > 0) {
			List<ScrollerData> _data;
			_data = new List<ScrollerData> ();

			//string leaderString = "";
			int ch = 0;
			foreach (var childSnapshot in args.Snapshot.Children) {
				if (childSnapshot.Child ("score") == null
				    || childSnapshot.Child ("score").Value == null) {
					Debug.LogError ("Bad data in sample.  Did you forget to call SetEditorDatabaseUrl with your project id?");
					break;
				} else {


					//leaderString = leaderString + ("\n" + childSnapshot.Child ("username").Value.ToString () + " - " + childSnapshot.Child ("score").Value.ToString ());
					string theName = childSnapshot.Child("username").Value.ToString();
					string theScore = childSnapshot.Child ("score").Value.ToString ();
					string firstWord = theName.Split(' ')[0];
					int avIn = int.Parse(childSnapshot.Child ("avatarIndex").Value.ToString());
					int clrIn = int.Parse(childSnapshot.Child ("colorIndex").Value.ToString());

					_data.Add (new ScrollerData () {
						
						leaderName = firstWord,
						leaderScore = theScore,
						leaderAvatarIndex = avIn,
						leaderAvatarColorIndex = clrIn
					});
				}
			}

			List<ScrollerData> _data2;
			_data2 = new List<ScrollerData> ();
			int posi = 1;
			for(var i = _data.Count - 1; i >= 0; i--){
				var theDat = _data[i];
				_data[i].leaderPosition = posi++;

				_data2.Add (theDat);
			}


			scrollContol.StartScroller (_data2);
			//LeaderText.text = leaderString;
			// Debug.Log ("Top " + args.Snapshot.ChildrenCount);
		}

	}
		

	public void LetsPlay(){
		if (loggedIn == 0) {
			User_Obj.SetActive (true);
			Splash_Obj.SetActive (false);
			MapIntro_Obj.SetActive (false);
			Map_Obj.SetActive (false);
			Leader_Obj.SetActive (false);
			AR_Camera_Obj.SetActive (false);

			User_Panel.blocksRaycasts = true;
			User_Panel.interactable = true;
			Avatar_Obj.SetActive (false);

			Fill_In_Blank_Obj.SetActive (false);
			MultipleChoice_Obj.SetActive (false);

		} else {
			auth.SignInAnonymouslyAsync().ContinueWith(task => {
				if (task.IsCanceled) {
					Debug.LogError("SignInAnonymouslyAsync was canceled.");
					return;
				}
				if (task.IsFaulted) {
					Debug.LogError("SignInAnonymouslyAsync encountered an error: " + task.Exception);
					return;
				}
					
				newUser = task.Result;
				Debug.LogFormat("User signed in successfully: {0} ({1})",
					newUser.DisplayName, newUser.UserId);


				FIB_1A = 0;
				FIB_1B = 0;
				FIB_2A = 0;
				FIB_2B = 0;

				MC_1 = 0;
				MC_2 = 0;
				MC_3 = 0;
				MC_4 = 0;
				MC_5 = 0;

				VID_1 = 0;
				VID_2 = 0;
				VID_3 = 0;
				VID_4 = 0;

				reference = FirebaseDatabase.DefaultInstance.RootReference;
				//reference.Child ("users").OrderByChild ("score").LimitToLast(100).ValueChanged += HandleLeadersChanged; 

				FirebaseDatabase.DefaultInstance
					.GetReference("users")
					.Child(newUser.UserId)
					.Child("FIB_1A")
					.GetValueAsync().ContinueWith(taskSubAll1 => {
						if (taskSubAll1.IsFaulted) {
							// Handle the error...
						}
						else if (taskSubAll1.IsCompleted) {
							DataSnapshot snapshot = taskSubAll1.Result;
							// Debug.Log(snapshot.Value.ToString());
							// Do something with snapshot...
							FIB_1A = int.Parse(snapshot.Value.ToString());




						}
					});

				FirebaseDatabase.DefaultInstance
					.GetReference("users")
					.Child(newUser.UserId)
					.Child("FIB_1B")
					.GetValueAsync().ContinueWith(taskSubAll2 => {
						if (taskSubAll2.IsFaulted) {
							// Handle the error...
						}
						else if (taskSubAll2.IsCompleted) {
							DataSnapshot snapshot = taskSubAll2.Result;
							// Debug.Log(snapshot.Value.ToString());
							// Do something with snapshot...
							FIB_1B = int.Parse(snapshot.Value.ToString());




						}
					});
				FirebaseDatabase.DefaultInstance
					.GetReference("users")
					.Child(newUser.UserId)
					.Child("FIB_2A")
					.GetValueAsync().ContinueWith(taskSubAll3 => {
						if (taskSubAll3.IsFaulted) {
							// Handle the error...
						}
						else if (taskSubAll3.IsCompleted) {
							DataSnapshot snapshot = taskSubAll3.Result;
							// Debug.Log(snapshot.Value.ToString());
							// Do something with snapshot...
							FIB_2A = int.Parse(snapshot.Value.ToString());




						}
					});
				FirebaseDatabase.DefaultInstance
					.GetReference("users")
					.Child(newUser.UserId)
					.Child("FIB_2B")
					.GetValueAsync().ContinueWith(taskSubAll4 => {
						if (taskSubAll4.IsFaulted) {
							// Handle the error...
						}
						else if (taskSubAll4.IsCompleted) {
							DataSnapshot snapshot = taskSubAll4.Result;
							// Debug.Log(snapshot.Value.ToString());
							// Do something with snapshot...
							FIB_2B = int.Parse(snapshot.Value.ToString());




						}
					});




				FirebaseDatabase.DefaultInstance
					.GetReference("users")
					.Child(newUser.UserId)
					.Child("MC_1")
					.GetValueAsync().ContinueWith(taskSubAll11 => {
						if (taskSubAll11.IsFaulted) {
							// Handle the error...
						}
						else if (taskSubAll11.IsCompleted) {
							DataSnapshot snapshot = taskSubAll11.Result;
							// Debug.Log(snapshot.Value.ToString());
							// Do something with snapshot...
							MC_1 = int.Parse(snapshot.Value.ToString());




						}
					});

				FirebaseDatabase.DefaultInstance
					.GetReference("users")
					.Child(newUser.UserId)
					.Child("MC_2")
					.GetValueAsync().ContinueWith(taskSubAll12 => {
						if (taskSubAll12.IsFaulted) {
							// Handle the error...
						}
						else if (taskSubAll12.IsCompleted) {
							DataSnapshot snapshot = taskSubAll12.Result;
							// Debug.Log(snapshot.Value.ToString());
							// Do something with snapshot...
							MC_2 = int.Parse(snapshot.Value.ToString());




						}
					});

		
				FirebaseDatabase.DefaultInstance
					.GetReference("users")
					.Child(newUser.UserId)
					.Child("MC_3")
					.GetValueAsync().ContinueWith(taskSubAll13 => {
						if (taskSubAll13.IsFaulted) {
							// Handle the error...
						}
						else if (taskSubAll13.IsCompleted) {
							DataSnapshot snapshot = taskSubAll13.Result;
							// Debug.Log(snapshot.Value.ToString());
							// Do something with snapshot...
							MC_3 = int.Parse(snapshot.Value.ToString());




						}
					});

				FirebaseDatabase.DefaultInstance
					.GetReference("users")
					.Child(newUser.UserId)
					.Child("MC_4")
					.GetValueAsync().ContinueWith(taskSubAll14 => {
						if (taskSubAll14.IsFaulted) {
							// Handle the error...
						}
						else if (taskSubAll14.IsCompleted) {
							DataSnapshot snapshot = taskSubAll14.Result;
							// Debug.Log(snapshot.Value.ToString());
							// Do something with snapshot...
							MC_4 = int.Parse(snapshot.Value.ToString());




						}
					});

				FirebaseDatabase.DefaultInstance
					.GetReference("users")
					.Child(newUser.UserId)
					.Child("MC_5")
					.GetValueAsync().ContinueWith(taskSubAll15 => {
						if (taskSubAll15.IsFaulted) {
							// Handle the error...
						}
						else if (taskSubAll15.IsCompleted) {
							DataSnapshot snapshot = taskSubAll15.Result;
							// Debug.Log(snapshot.Value.ToString());
							// Do something with snapshot...
							MC_5 = int.Parse(snapshot.Value.ToString());




						}
					});

				/////////////////////////

				FirebaseDatabase.DefaultInstance
					.GetReference("users")
					.Child(newUser.UserId)
					.Child("VID_1")
					.GetValueAsync().ContinueWith(taskSubAll1v => {
						if (taskSubAll1v.IsFaulted) {
							// Handle the error...
						}
						else if (taskSubAll1v.IsCompleted) {
							DataSnapshot snapshot = taskSubAll1v.Result;
							// Debug.Log(snapshot.Value.ToString());
							// Do something with snapshot...
							VID_1 = int.Parse(snapshot.Value.ToString());




						}
					});

				FirebaseDatabase.DefaultInstance
					.GetReference("users")
					.Child(newUser.UserId)
					.Child("VID_2")
					.GetValueAsync().ContinueWith(taskSubAll2v => {
						if (taskSubAll2v.IsFaulted) {
							// Handle the error...
						}
						else if (taskSubAll2v.IsCompleted) {
							DataSnapshot snapshot = taskSubAll2v.Result;
							// Debug.Log(snapshot.Value.ToString());
							// Do something with snapshot...
							VID_2 = int.Parse(snapshot.Value.ToString());




						}
					});
				FirebaseDatabase.DefaultInstance
					.GetReference("users")
					.Child(newUser.UserId)
					.Child("VID_3")
					.GetValueAsync().ContinueWith(taskSubAll3v => {
						if (taskSubAll3v.IsFaulted) {
							// Handle the error...
						}
						else if (taskSubAll3v.IsCompleted) {
							DataSnapshot snapshot = taskSubAll3v.Result;
							// Debug.Log(snapshot.Value.ToString());
							// Do something with snapshot...
							VID_3 = int.Parse(snapshot.Value.ToString());




						}
					});
				
				FirebaseDatabase.DefaultInstance
					.GetReference("users")
					.Child(newUser.UserId)
					.Child("VID_4")
					.GetValueAsync().ContinueWith(taskSubAll4v => {
						if (taskSubAll4v.IsFaulted) {
							// Handle the error...
						}
						else if (taskSubAll4v.IsCompleted) {
							DataSnapshot snapshot = taskSubAll4v.Result;
							// Debug.Log(snapshot.Value.ToString());
							// Do something with snapshot...
							VID_4 = int.Parse(snapshot.Value.ToString());




						}
					});

				//////////////////////

				// Debug.Log(newUser.UserId.ToString());
			
				FirebaseDatabase.DefaultInstance
					.GetReference("users")
					.Child(newUser.UserId)
					.Child("score")
					.GetValueAsync().ContinueWith(taskSub => {
						if (taskSub.IsFaulted) {
							// Handle the error...
						}
						else if (taskSub.IsCompleted) {
							DataSnapshot snapshot = taskSub.Result;
							// Debug.Log(snapshot.Value.ToString());
							// Do something with snapshot...

							introMapScore.text = snapshot.Value.ToString()+" pts";
							mapScore.text  = snapshot.Value.ToString()+" pts";
							NetPointsText.text  = snapshot.Value.ToString()+" pts";
							MolesNetPointsText.text = snapshot.Value.ToString()+" pts";
							currScore = int.Parse(snapshot.Value.ToString());
						}
					});


				FirebaseDatabase.DefaultInstance
					.GetReference("users")
					.Child(newUser.UserId)
					.Child("username")
					.GetValueAsync().ContinueWith(taskSub2 => {
						if (taskSub2.IsFaulted) {
							// Handle the error...
						}
						else if (taskSub2.IsCompleted) {
							DataSnapshot snapshot2 = taskSub2.Result;
							// Debug.Log(snapshot2.Value.ToString());
							// Do something with snapshot...

							currUserName  = (snapshot2.Value.ToString());
						}
					});

				FirebaseDatabase.DefaultInstance
					.GetReference("users")
					.Child(newUser.UserId)
					.Child("avatarIndex")
					.GetValueAsync().ContinueWith(taskSub3 => {
						if (taskSub3.IsFaulted) {
							// Handle the error...
						}
						else if (taskSub3.IsCompleted) {
							DataSnapshot snapshot3 = taskSub3.Result;
							// Debug.Log(snapshot3.Value.ToString());
							// Do something with snapshot...

							currAvatar = int.Parse(snapshot3.Value.ToString());
							SetAvatar(currAvatar);
					
						}
					});

				FirebaseDatabase.DefaultInstance
					.GetReference("users")
					.Child(newUser.UserId)
					.Child("colorIndex")
					.GetValueAsync().ContinueWith(taskSub4 => {
						if (taskSub4.IsFaulted) {
							// Handle the error...
						}
						else if (taskSub4.IsCompleted) {
							DataSnapshot snapshot4 = taskSub4.Result;
							// Debug.Log(snapshot4.Value.ToString());
							// Do something with snapshot...

							currColor  = int.Parse(snapshot4.Value.ToString());
							switchColor(currColor);
						}
					});


				FirebaseDatabase.DefaultInstance
					.GetReference("users")
					.Child(newUser.UserId)
					.Child("email")
					.GetValueAsync().ContinueWith(taskSub5 => {
						if (taskSub5.IsFaulted) {
							// Handle the error...
						}
						else if (taskSub5.IsCompleted) {
							DataSnapshot snapshot5 = taskSub5.Result;
							// Debug.Log(snapshot5.Value.ToString());
							// Do something with snapshot...

							settingsEmail.text  = snapshot5.Value.ToString();
						}
					});
				
//				FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://citi-mvp-leadership-retreat.firebaseio.com/");
//				reference = FirebaseDatabase.DefaultInstance.RootReference;
//				DatabaseReference refe = FirebaseDatabase.DefaultInstance.GetReference("users/"+newUser.UserId+"/score");
//
//				string currScore = reference.Child("users").Child(newUser.UserId).Child("score").GetValueAsync().ToString();
//
//
//				int theScore = int.Parse(currScore);
				//	.GetReference("users").Child(newUser.UserId).Child("score").GetValueAsync().Result.Value;
//				User user = new User(NameText.text, EmailText.text, theScore, newUser.UserId);
//				string json = JsonUtility.ToJson(user);
//				reference.Child("users").Child(newUser.UserId).SetRawJsonValueAsync(json);

				// Debug.Log("bypassing with old score of ");

				GoMapIntro();


			});
		}
	}

	public void LogOutOfFireBase(){

		auth.SignOut ();

		LetsStart ();



	}

	public void updateScore(int addScore){
		// Debug.Log ("updating score currscore =" + currScore);
		currScore = currScore + addScore;
		if (reference == null) {
			reference = FirebaseDatabase.DefaultInstance.RootReference;
		}
			reference.Child ("users").Child (newUser.UserId).Child ("score").SetValueAsync (currScore);
		reference.Child("users").Child(newUser.UserId).Child("avatarIndex").SetValueAsync(currAvatar);
        reference.Child("users").Child(newUser.UserId).Child("colorIndex").SetValueAsync(currColor);
        reference.Child("users").Child(newUser.UserId).Child("username").SetValueAsync(currUserName);
		
		mapScore.text = currScore.ToString ()+" pts";
		introMapScore.text = currScore.ToString ()+" pts";
		NetPointsText.text  = currScore.ToString()+" pts";
		MolesNetPointsText.text = currScore.ToString()+" pts";

	}


	public void updateFinished(string section, int subsection){
		// Debug.Log ("updating finished section =" + section);
	
		if (reference == null) {
			reference = FirebaseDatabase.DefaultInstance.RootReference;
		}
		reference.Child ("users").Child (newUser.UserId).Child (section).SetValueAsync (subsection);


	}

	public void updateVideoStateInternal(){
		if (video_1.isDone == true) {
			VID_1 = 1;
		} else {
			videoPlayer_1.frame = 0;
		}

		if (video_2.isDone == true) {
			VID_2 = 1;
		} else {
			videoPlayer_2.frame = 0;
		}

		if (video_3.isDone == true) {
			VID_3 = 1;
		} else {
			videoPlayer_3.frame = 0;
		}
		if (video_4.isDone == true) {
			VID_4 = 1;
		} else {
			videoPlayer_4.frame = 0;
		}


	}

	public void GoAvatar(){


		if ((MeetingCode.text).ToLower() == "bethebest" && NameText.text.Trim() != "" && EmailText.text.Trim() != "") {
			
		

			int theScore = 0; //int.Parse(ScoreText.text);
			// Debug.Log ("loggedin=" + loggedIn);
			if (loggedIn == 0) {
				auth.SignInAnonymouslyAsync ().ContinueWith (task => {
					if (task.IsCanceled) {
						Debug.LogError ("SignInAnonymouslyAsync was canceled.");
						return;
					}
					if (task.IsFaulted) {
						Debug.LogError ("SignInAnonymouslyAsync encountered an error: " + task.Exception);
						return;
					}

					newUser = task.Result;
					Debug.LogFormat ("User signed in successfully: {0} ({1})",
						newUser.DisplayName, newUser.UserId);


					// Debug.Log (newUser.UserId.ToString ());
					loggedIn = 1;

					FirebaseApp.DefaultInstance.SetEditorDatabaseUrl ("https://citi-mvp-leadership-retreat.firebaseio.com/");
					reference = FirebaseDatabase.DefaultInstance.RootReference;


					User user = new User (NameText.text, EmailText.text, theScore, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
					string json = JsonUtility.ToJson (user);
					reference.Child ("users").Child (newUser.UserId).SetRawJsonValueAsync (json);
					//reference.Child ("users").OrderByChild ("score").LimitToLast(100).ValueChanged += HandleLeadersChanged;
					//subscribedToLeaders = true;

				 });
			}

			//PlayerPrefs.SetInt ("Authorized", loggedIn);

			Avatar_Obj.SetActive (true);
			User_Obj.SetActive (false);
			Splash_Obj.SetActive (false);
			MapIntro_Obj.SetActive (false);
			Map_Obj.SetActive (false);
			Leader_Obj.SetActive (false);
			AR_Camera_Obj.SetActive (false);

			Fill_In_Blank_Obj.SetActive (false);
			MultipleChoice_Obj.SetActive (false);

			Avatar_Panel.blocksRaycasts = true;
			Avatar_Panel.interactable = true;
			currUserName = NameText.text;
			string firstWord = currUserName.Split(' ')[0];
			settingsEmail.text = EmailText.text;
			settingsName.text = currUserName;
			MapIntroNameText.text = firstWord;
			MapNameText.text = firstWord;
			SetAvatarText.text = "Hey " + (firstWord) + "!"  + "\nLet's set your leaderboard avatar";


		} else {

			// Debug.Log ("WrongCode");

		}


	}

	public void GoMapIntroNew(){

		// Debug.Log ("Going new Intro");
		Avatar_Obj.SetActive (false);
		User_Obj.SetActive (false);
		Splash_Obj.SetActive (false);
		MapIntro_Obj.SetActive (true);
		Map_Obj.SetActive (false);
		Leader_Obj.SetActive (false);
		AR_Camera_Obj.SetActive (false);

		Fill_In_Blank_Obj.SetActive (false);
		MultipleChoice_Obj.SetActive (false);

		MapIntroNameText.text = currUserName.Split(' ')[0];
		MapNameText.text = currUserName.Split (' ') [0];
		settingsName.text = currUserName;


		MapIntro_Panel.blocksRaycasts = true;
		MapIntro_Panel.interactable = true;

		int cavatar = currAvatar;
		string json = JsonUtility.ToJson(cavatar);

		int colorindx = currColor;
		string jsonColor = JsonUtility.ToJson (colorindx);

		reference.Child("users").Child(newUser.UserId).Child("avatarIndex").SetValueAsync(currAvatar);
		reference.Child("users").Child(newUser.UserId).Child("colorIndex").SetValueAsync(currColor);


		// currAvatar
		// currColor



	}
	public void GoMapIntro(){

		// Debug.Log ("Going Map Intro");

		Avatar_Obj.SetActive (false);
		User_Obj.SetActive (false);
		Splash_Obj.SetActive (false);
		MapIntro_Obj.SetActive (true);
		Map_Obj.SetActive (false);
		Leader_Obj.SetActive (false);
		AR_Camera_Obj.SetActive (false);

		Fill_In_Blank_Obj.SetActive (false);
		MultipleChoice_Obj.SetActive (false);
		MapNameText.text = currUserName.Split(' ')[0];
		MapIntroNameText.text = currUserName.Split(' ')[0];
		settingsName.text = currUserName;

		MapIntro_Panel.blocksRaycasts = true;
		MapIntro_Panel.interactable = true;


	}

	public void GoMap(){
		ARmode = false;
		vidCompletePop.SetActive (false);
		if (subscribedToLeaders == true) {
			reference.Child ("users").OrderByChild("score").LimitToLast (100).ValueChanged -= HandleLeadersChanged; 
			subscribedToLeaders = false;
		}

		if (videoPlayer_1.isPlaying  == true|| videoPlayer_2.isPlaying  == true ||
			videoPlayer_3.isPlaying  == true || videoPlayer_4.isPlaying  == true) {
			videoPlayer_1.Pause ();

			videoPlayer_2.Pause ();

			videoPlayer_3.Pause ();

			videoPlayer_4.Pause ();
		}


//		if (videoPlayer_1.GetCurrentState()  == MediaPlayerCtrl.MEDIAPLAYER_STATE.PLAYING|| videoPlayer_2.GetCurrentState()  == MediaPlayerCtrl.MEDIAPLAYER_STATE.PLAYING ||
//			videoPlayer_3.GetCurrentState()  == MediaPlayerCtrl.MEDIAPLAYER_STATE.PLAYING || videoPlayer_4.GetCurrentState()  == MediaPlayerCtrl.MEDIAPLAYER_STATE.PLAYING) {
//			videoPlayer_1.Pause ();
//
//			videoPlayer_2.Pause ();
//
//			videoPlayer_3.Pause ();
//
//			videoPlayer_4.Pause ();
//		}


		basketBall.SetActive (false);
		GetYourGoals.SetActive (false);
		Avatar_Obj.SetActive (false);
		User_Obj.SetActive (false);
		Splash_Obj.SetActive (false);
		MapIntro_Obj.SetActive (false);
		Map_Obj.SetActive (true);
		Leader_Obj.SetActive (false);
		AR_Camera_Obj.SetActive (false);
		AR_Back_Button.SetActive (true);
		Instructions.SetActive (false);
		Leaderboards.SetActive (false);
		Settings.SetActive (false);
		NothinButNetPanel.SetActive (false);

		Fill_In_Blank_Obj.SetActive (false);
		MultipleChoice_Obj.SetActive (false);

		MapNameText.text = currUserName.Split(' ')[0];
		settingsName.text = currUserName;

		Map_Panel.blocksRaycasts = true;
		Map_Panel.interactable = true;

	}

	public void GoSettings(){

		Settings.SetActive (true);

		Avatar_Obj.SetActive (false);
		User_Obj.SetActive (false);
		Splash_Obj.SetActive (false);
		MapIntro_Obj.SetActive (false);
		Map_Obj.SetActive (false);
		Leader_Obj.SetActive (false);
		AR_Camera_Obj.SetActive (false);

		Fill_In_Blank_Obj.SetActive (false);
		MultipleChoice_Obj.SetActive (false);
	}

		
	public void GoLeader() {

		Avatar_Obj.SetActive (false);
		User_Obj.SetActive (false);
		Splash_Obj.SetActive (false);
		MapIntro_Obj.SetActive (false);
		Map_Obj.SetActive (false);
		Leader_Obj.SetActive (true);
		AR_Camera_Obj.SetActive (false);

		Fill_In_Blank_Obj.SetActive (false);
		MultipleChoice_Obj.SetActive (false);

		Leader_Panel.blocksRaycasts = true;
		Leader_Panel.interactable = true;

	}


	public void GoAR() {
		Avatar_Obj.SetActive (false);
		User_Obj.SetActive (false);
		Splash_Obj.SetActive (false);
		MapIntro_Obj.SetActive (false);
		Map_Obj.SetActive (false);
		Leader_Obj.SetActive (false);
		AR_Camera_Obj.SetActive (true);

		Fill_In_Blank_Obj.SetActive (false);
		MultipleChoice_Obj.SetActive (false);

		AR_Camera_Panel.blocksRaycasts = true;
		AR_Camera_Panel.interactable = true;

		AR_Back_Button.SetActive (true);



		if (VID_1 == 1) {
			video_1.isDone = true;
		} else {
			video_1.isDone = false;
		}


		if (VID_2 == 1) {
			video_2.isDone = true;
		} else {
			video_2.isDone = false;
		}

		if (VID_3 == 1) {
			video_3.isDone = true;
		} else {
			video_3.isDone = false;
		}

		if (VID_4 == 1) {
			video_4.isDone = true;
		} else {
			video_4.isDone = false;
		}




		ARmode = true;

	}

	public void check_FIN_1a(InputField theAnswer){
		if (theAnswer.text.ToLower() == "growth") {
			Fill_In_Blank_1A_Right.SetActive (true);
			Fill_In_Blank_1A_Wrong.SetActive (false);
			int plusScore = 0;
			plusScore = 10+ (int)(FIB_1a_timer.getBonus());
			FIB_1a_plus_text.text = "+ " + (plusScore.ToString ());
			updateScore (plusScore);
			updateFinished ("FIB_1A", 1);
			FIB_1A = 1;

		} else {
			Fill_In_Blank_1A_Wrong.SetActive (true);
			Fill_In_Blank_1A_Right.SetActive (false);
		}
	}

	public void check_FIN_1b(InputField theAnswer){
		if (theAnswer.text.ToLower() == "best") {
			Fill_In_Blank_1B_Right.SetActive (true);
			Fill_In_Blank_1B_Wrong.SetActive (false);
			int plusScore = 0;
			plusScore = 10+ (int)(FIB_1b_timer.getBonus());
			FIB_1b_plus_text.text = "+ " + (plusScore.ToString ());
			updateScore (plusScore);
			updateFinished ("FIB_1B", 1);
			FIB_1B = 1;
		} else {
			Fill_In_Blank_1B_Wrong.SetActive (true);
			Fill_In_Blank_1B_Right.SetActive (false);
		}
	}

	public void check_FIN_2A(InputField theAnswer){
		if (theAnswer.text.ToLower() == "social") {
			Fill_In_Blank_2A_Right.SetActive (true);
			Fill_In_Blank_2A_Wrong.SetActive (false);
			int plusScore = 0;
			plusScore = 10+ (int)(FIB_2a_timer.getBonus());
			FIB_2a_plus_text.text = "+ " + (plusScore.ToString ());
			updateScore (plusScore);
			updateFinished ("FIB_2A", 1);
			FIB_2A = 1;
		} else {
			Fill_In_Blank_2A_Wrong.SetActive (true);
			Fill_In_Blank_2A_Right.SetActive (false);
		}
	}

	public void check_FIN_2B(InputField theAnswer){
		if (theAnswer.text.ToLower() == "3") {
			Fill_In_Blank_2B_Right.SetActive (true);
			Fill_In_Blank_2B_Wrong.SetActive (false);
			int plusScore = 0;
			plusScore = 10+ (int)(FIB_2b_timer.getBonus());
			FIB_2b_plus_text.text = "+ " + (plusScore.ToString ());
			updateScore (plusScore);
			updateFinished ("FIB_2B", 1);
			FIB_2B = 1;
		} else {
			Fill_In_Blank_2B_Right.SetActive (true);
			Fill_In_Blank_2B_Right.SetActive (false);
		}
	}


	public void FIN_1_Done(){
		Fill_In_Blank_1_Complete.SetActive (true);
	}

	public void FIN_2_Done(){
		Fill_In_Blank_2_Complete.SetActive (true);
	}

	public void GoFillIn(){
		Avatar_Obj.SetActive (false);
		User_Obj.SetActive (false);
		Splash_Obj.SetActive (false);
		MapIntro_Obj.SetActive (false);
		Map_Obj.SetActive (false);
		Leader_Obj.SetActive (false);
		AR_Camera_Obj.SetActive (false);


		if (FIB_1A == 0) {

			Fill_In_Blank_1A_Obj.SetActive (true);
			Fill_In_Blank_1B_Obj.SetActive (false);

		} else {

			if (FIB_1B == 0) {
				Fill_In_Blank_1A_Obj.SetActive (false);
				Fill_In_Blank_1B_Obj.SetActive (true);

			} else {
				Fill_In_Blank_1A_Obj.SetActive (false);
				Fill_In_Blank_1B_Obj.SetActive (false);

				Fill_In_Blank_1_Complete.SetActive (true);

			}
		}


		Fill_In_Blank_1A_Right.SetActive (false);
		Fill_In_Blank_1A_Wrong.SetActive (false);

		Fill_In_Blank_1B_Right.SetActive (false);
		Fill_In_Blank_1B_Wrong.SetActive (false);

		Fill_In_Blank_2A_Right.SetActive (false);
		Fill_In_Blank_2A_Wrong.SetActive (false);

		Fill_In_Blank_2B_Right.SetActive (false);
		Fill_In_Blank_2B_Wrong.SetActive (false);

		Fill_In_Blank_Obj.SetActive (true);

		Fill_In_Blank_1_Obj.SetActive (true);
		Fill_In_Blank_2_Obj.SetActive (false);


		MultipleChoice_Obj.SetActive (false);


	}

	public void GoFillIn_1_b(){
		Fill_In_Blank_Obj.SetActive (true);
		Fill_In_Blank_1_Obj.SetActive (true);
		Fill_In_Blank_1A_Obj.SetActive (false);
		Fill_In_Blank_1B_Obj.SetActive (true);

		Fill_In_Blank_1A_Right.SetActive (false);
		Fill_In_Blank_1A_Wrong.SetActive (false);


		Fill_In_Blank_2_Obj.SetActive (false);
		Fill_In_Blank_2A_Obj.SetActive (false);
		Fill_In_Blank_2B_Obj.SetActive (false);

	}


	public void GoFillIn_2(){
		Avatar_Obj.SetActive (false);
		User_Obj.SetActive (false);
		Splash_Obj.SetActive (false);
		MapIntro_Obj.SetActive (false);
		Map_Obj.SetActive (false);
		Leader_Obj.SetActive (false);
		AR_Camera_Obj.SetActive (false);


		if (FIB_2A == 0) {

			Fill_In_Blank_2A_Obj.SetActive (true);
			Fill_In_Blank_2B_Obj.SetActive (false);

		} else {

			if (FIB_2B == 0) {
				Fill_In_Blank_2A_Obj.SetActive (false);
				Fill_In_Blank_2B_Obj.SetActive (true);

			} else {
				Fill_In_Blank_2A_Obj.SetActive (false);
				Fill_In_Blank_2B_Obj.SetActive (false);

				Fill_In_Blank_2_Complete.SetActive (true);

			}
		}


		Fill_In_Blank_1A_Right.SetActive (false);
		Fill_In_Blank_1A_Wrong.SetActive (false);

		Fill_In_Blank_1B_Right.SetActive (false);
		Fill_In_Blank_1B_Wrong.SetActive (false);

		Fill_In_Blank_2A_Right.SetActive (false);
		Fill_In_Blank_2A_Wrong.SetActive (false);

		Fill_In_Blank_2B_Right.SetActive (false);
		Fill_In_Blank_2B_Wrong.SetActive (false);


		Fill_In_Blank_Obj.SetActive (true);

		Fill_In_Blank_1_Obj.SetActive (false);
		Fill_In_Blank_2_Obj.SetActive (true);


		MultipleChoice_Obj.SetActive (false);


	}


	public void GoFillIn_2_b(){
		Fill_In_Blank_Obj.SetActive (false);
		Fill_In_Blank_1A_Obj.SetActive (false);
		Fill_In_Blank_1B_Obj.SetActive (false);
		Fill_In_Blank_1_Obj.SetActive (false);

		Fill_In_Blank_2_Obj.SetActive (true);
		Fill_In_Blank_2A_Obj.SetActive (false);
		Fill_In_Blank_2B_Obj.SetActive (true);

	}


	public void GoMultipleChoice(int whichQuiz){
		Avatar_Obj.SetActive (false);
		User_Obj.SetActive (false);
		Splash_Obj.SetActive (false);
		MapIntro_Obj.SetActive (false);
		Map_Obj.SetActive (false);
		Leader_Obj.SetActive (false);
		AR_Camera_Obj.SetActive (false);
		Fill_In_Blank_Obj.SetActive (false);

		MultipleChoice_1a_Right.SetActive (false);
		MultipleChoice_1a_Wrong.SetActive (false);

		MultipleChoice_2a_Right.SetActive (false);
		MultipleChoice_2a_Wrong.SetActive (false);

		MultipleChoice_1b_Right.SetActive (false);
		MultipleChoice_1b_Wrong.SetActive (false);

		MultipleChoice_2b_Right.SetActive (false);
		MultipleChoice_2b_Wrong.SetActive (false);


		MultipleChoice_3a_Right.SetActive (false);
		MultipleChoice_3a_Wrong.SetActive (false);

		MultipleChoice_3b_Right.SetActive (false);
		MultipleChoice_3b_Wrong.SetActive (false);

		MultipleChoice_4a_Right.SetActive (false);
		MultipleChoice_4a_Wrong.SetActive (false);

		MultipleChoice_4b_Right.SetActive (false);
		MultipleChoice_4b_Wrong.SetActive (false);

		MultipleChoice_5a_Right.SetActive (false);
		MultipleChoice_5a_Wrong.SetActive (false);

		MultipleChoice_5b_Right.SetActive (false);
		MultipleChoice_5b_Wrong.SetActive (false);


		//MultipleChoice_1_Complete.SetActive (false);


		MultipleChoice_Obj.SetActive (true);

		if (whichQuiz == 0) {
			MultipleChoice_1_Obj.SetActive (true);
			curr_MC = MultipleChoice_1b_Obj;


			if (MC_1 == 0) {
				MultipleChoice_1a_Obj.SetActive (true);
				MultipleChoice_1b_Obj.SetActive (false);

			} else {
				if (MC_1 == 1) {

					MultipleChoice_1a_Obj.SetActive (false);
					MultipleChoice_1b_Obj.SetActive (true);

				} else {

					if (MC_1 == 2) {

						MultipleChoice_1a_Obj.SetActive (false);
						MultipleChoice_1b_Obj.SetActive (false);

						MultipleChoice_1_Complete.SetActive (true);

					}
				}
			}

		} else {
			MultipleChoice_1_Obj.SetActive (false);
		}

		if (whichQuiz == 1) {
			MultipleChoice_2_Obj.SetActive (true);
			curr_MC = MultipleChoice_2b_Obj;



			if (MC_2 == 0) {
				MultipleChoice_2a_Obj.SetActive (true);
				MultipleChoice_2b_Obj.SetActive (false);

			} else {
				if (MC_2 == 1) {

					MultipleChoice_2a_Obj.SetActive (false);
					MultipleChoice_2b_Obj.SetActive (true);

				} else {

					if (MC_2 == 2) {

						MultipleChoice_2a_Obj.SetActive (false);
						MultipleChoice_2b_Obj.SetActive (false);

						MultipleChoice_2_Complete.SetActive (true);

					}
				}
			}



		} else {
			MultipleChoice_2_Obj.SetActive (false);
		}



		if (whichQuiz == 2) {
			MultipleChoice_3_Obj.SetActive (true);
			curr_MC = MultipleChoice_3b_Obj;


			if (MC_3 == 0) {
				MultipleChoice_3a_Obj.SetActive (true);
				MultipleChoice_3b_Obj.SetActive (false);

			} else {
				if (MC_3 == 1) {

					MultipleChoice_3a_Obj.SetActive (false);
					MultipleChoice_3b_Obj.SetActive (true);

				} else {

					if (MC_3 == 2) {

						MultipleChoice_3a_Obj.SetActive (false);
						MultipleChoice_3b_Obj.SetActive (false);

						MultipleChoice_3_Complete.SetActive (true);

					}
				}
			}


		} else {
			MultipleChoice_3_Obj.SetActive (false);
		}

		if (whichQuiz == 3) {
			MultipleChoice_4_Obj.SetActive (true);
			curr_MC = MultipleChoice_4b_Obj;


			if (MC_4 == 0) {
				MultipleChoice_4a_Obj.SetActive (true);
				MultipleChoice_4b_Obj.SetActive (false);

			} else {
				if (MC_4 == 1) {

					MultipleChoice_4a_Obj.SetActive (false);
					MultipleChoice_4b_Obj.SetActive (true);

				} else {

					if (MC_4 == 2) {

						MultipleChoice_4a_Obj.SetActive (false);
						MultipleChoice_4b_Obj.SetActive (false);

						MultipleChoice_4_Complete.SetActive (true);

					}
				}
			}

		} else {
			MultipleChoice_4_Obj.SetActive (false);
		}

		if (whichQuiz == 4) {
			MultipleChoice_5_Obj.SetActive (true);
			curr_MC = MultipleChoice_5b_Obj;


			if (MC_5 == 0) {
				MultipleChoice_5a_Obj.SetActive (true);
				MultipleChoice_5b_Obj.SetActive (false);

			} else {
				if (MC_5 == 1) {

					MultipleChoice_5a_Obj.SetActive (false);
					MultipleChoice_5b_Obj.SetActive (true);

				} else {

					if (MC_5 == 2) {

						MultipleChoice_5a_Obj.SetActive (false);
						MultipleChoice_5b_Obj.SetActive (false);

						MultipleChoice_5_Complete.SetActive (true);

					}
				}
			}


		} else {
			MultipleChoice_5_Obj.SetActive (false);
		}




	}


	public void go_MC_1b(){
		MultipleChoice_1b_Obj.SetActive (true);
		MultipleChoice_1a_Obj.SetActive (false);
		//MultipleChoice_1a_Right.SetActive (false);

	}

	public void MC_1a_Wrong(){
		MultipleChoice_1b_Obj.SetActive (false);
		MultipleChoice_1a_Obj.SetActive (true);
		MultipleChoice_1a_Wrong.SetActive (false);

	}

	public void MC_1b_Wrong(){
		MultipleChoice_1b_Obj.SetActive (true);
		MultipleChoice_1a_Obj.SetActive (false);
		MultipleChoice_1b_Wrong.SetActive (false);

	}

	public void MC_1_Done(){
		MultipleChoice_1_Complete.SetActive (true);
	}
		

	public void check_MC_1a(Toggle theAnswer){
		if (theAnswer.isOn == true) {
			MultipleChoice_1a_Right.SetActive (true);
			MultipleChoice_1a_Wrong.SetActive (false);
			int plusScore = 0;
			plusScore = 10+ (int)(MC_1a_timer.getBonus());
			MC_1a_plus_text.text = "+ " + (plusScore.ToString ());
			updateScore (plusScore);
			updateFinished ("MC_1", 1);
			MC_1 = 1;
		} else {
			MultipleChoice_1a_Wrong.SetActive (true);
			MultipleChoice_1a_Right.SetActive (false);
		}
	}

	public void check_MC_1b(Toggle theAnswer){
		if (theAnswer.isOn == true) {
			MultipleChoice_1b_Right.SetActive (true);
			MultipleChoice_1b_Wrong.SetActive (false);
			int plusScore = 0;
			plusScore = 10+ (int)(MC_1b_timer.getBonus());
			MC_1b_plus_text.text = "+ " + (plusScore.ToString ());
			updateScore (plusScore);
			updateFinished ("MC_1", 2);
			MC_1 = 2;

		} else {
			MultipleChoice_1b_Wrong.SetActive (true);
			MultipleChoice_1b_Right.SetActive (false);

		}
	}

	// MC_2

	public void go_MC_2b(){
		MultipleChoice_2b_Obj.SetActive (true);
		MultipleChoice_2a_Obj.SetActive (false);
		//MultipleChoice_1a_Right.SetActive (false);

	}

	public void MC_2a_Wrong(){
		MultipleChoice_2b_Obj.SetActive (false);
		MultipleChoice_2a_Obj.SetActive (true);
		MultipleChoice_2a_Wrong.SetActive (false);

	}

	public void MC_2b_Wrong(){
		MultipleChoice_2b_Obj.SetActive (true);
		MultipleChoice_2a_Obj.SetActive (false);
		MultipleChoice_2b_Wrong.SetActive (false);

	}

	public void MC_2_Done(){
		MultipleChoice_2_Complete.SetActive (true);
	}


	public void check_MC_2a(Toggle theAnswer){
		if (theAnswer.isOn == true) {
			MultipleChoice_2a_Right.SetActive (true);
			MultipleChoice_2a_Wrong.SetActive (false);
			int plusScore = 0;
			plusScore = 10+ (int)(MC_2a_timer.getBonus());
			MC_2a_plus_text.text = "+ " + (plusScore.ToString ());
			updateScore (plusScore);
			updateFinished ("MC_2", 1);
			MC_2 = 1;
		} else {
			MultipleChoice_2a_Wrong.SetActive (true);
			MultipleChoice_2a_Right.SetActive (false);
		}
	}

	public void check_MC_2b(Toggle theAnswer){
		if (theAnswer.isOn == true) {
			MultipleChoice_2b_Right.SetActive (true);
			MultipleChoice_2b_Wrong.SetActive (false);
			int plusScore = 0;
			plusScore = 10+ (int)(MC_2b_timer.getBonus());
			MC_2b_plus_text.text = "+ " + (plusScore.ToString ());
			updateScore (plusScore);
			updateFinished ("MC_2", 2);
			MC_2 = 2;
		} else {
			MultipleChoice_2b_Wrong.SetActive (true);
			MultipleChoice_2b_Right.SetActive (false);

		}
	}


	// MC_3

	public void go_MC_3b(){
		MultipleChoice_3b_Obj.SetActive (true);
		MultipleChoice_3a_Obj.SetActive (false);
		//MultipleChoice_1a_Right.SetActive (false);

	}

	public void MC_3a_Wrong(){
		MultipleChoice_3b_Obj.SetActive (false);
		MultipleChoice_3a_Obj.SetActive (true);
		MultipleChoice_3a_Wrong.SetActive (false);

	}

	public void MC_3b_Wrong(){
		MultipleChoice_3b_Obj.SetActive (true);
		MultipleChoice_3a_Obj.SetActive (false);
		MultipleChoice_3b_Wrong.SetActive (false);

	}

	public void MC_3_Done(){
		MultipleChoice_3_Complete.SetActive (true);
	}



	public void check_MC_3a(Toggle theAnswer){
		if (theAnswer.isOn == true) {
			MultipleChoice_3a_Right.SetActive (true);
			MultipleChoice_3a_Wrong.SetActive (false);
			int plusScore = 0;
			plusScore = 10+ (int)(MC_3a_timer.getBonus());
			MC_3a_plus_text.text = "+ " + (plusScore.ToString ());
			updateScore (plusScore);
			updateFinished ("MC_3", 1);
			MC_3 = 1;
		} else {
			MultipleChoice_3a_Wrong.SetActive (true);
			MultipleChoice_3a_Right.SetActive (false);
		}
	}

	public void check_MC_3b(Toggle theAnswer){
		if (theAnswer.isOn == true) {
			MultipleChoice_3b_Right.SetActive (true);
			MultipleChoice_3b_Wrong.SetActive (false);
			int plusScore = 0;
			plusScore = 10+ (int)(MC_3b_timer.getBonus());
			MC_3b_plus_text.text = "+ " + (plusScore.ToString ());
			updateScore (plusScore);
			updateFinished ("MC_3", 2);
			MC_3 = 2;

		} else {
			MultipleChoice_3b_Wrong.SetActive (true);
			MultipleChoice_3b_Right.SetActive (false);

		}
	}

	// MC_4

	public void go_MC_4b(){
		MultipleChoice_4b_Obj.SetActive (true);
		MultipleChoice_4a_Obj.SetActive (false);
		//MultipleChoice_1a_Right.SetActive (false);

	}

	public void MC_4a_Wrong(){
		MultipleChoice_4b_Obj.SetActive (false);
		MultipleChoice_4a_Obj.SetActive (true);
		MultipleChoice_4a_Wrong.SetActive (false);

	}

	public void MC_4b_Wrong(){
		MultipleChoice_4b_Obj.SetActive (true);
		MultipleChoice_4a_Obj.SetActive (false);
		MultipleChoice_4b_Wrong.SetActive (false);

	}

	public void MC_4_Done(){
		MultipleChoice_4_Complete.SetActive (true);
	}


	public void check_MC_4a(Toggle theAnswer){
		if (theAnswer.isOn == true) {
			MultipleChoice_4a_Right.SetActive (true);
			MultipleChoice_4a_Wrong.SetActive (false);
			int plusScore = 0;
			plusScore = 10+ (int)(MC_4a_timer.getBonus());
			MC_4a_plus_text.text = "+ " + (plusScore.ToString ());
			updateScore (plusScore);
			updateFinished ("MC_4", 1);
			MC_4 = 1;
		} else {
			MultipleChoice_4a_Wrong.SetActive (true);
			MultipleChoice_4a_Right.SetActive (false);
		}
	}

	public void check_MC_4b(Toggle theAnswer){
		if (theAnswer.isOn == true) {
			MultipleChoice_4b_Right.SetActive (true);
			MultipleChoice_4b_Wrong.SetActive (false);
			int plusScore = 0;
			plusScore = 10+ (int)(MC_4b_timer.getBonus());
			MC_4b_plus_text.text = "+ " + (plusScore.ToString ());
			updateScore (plusScore);
			updateFinished ("MC_4", 2);
			MC_4 = 2;

		} else {
			MultipleChoice_4b_Wrong.SetActive (true);
			MultipleChoice_4b_Right.SetActive (false);

		}
	}

	// MC_5

	public void go_MC_5b(){
		MultipleChoice_5b_Obj.SetActive (true);
		MultipleChoice_5a_Obj.SetActive (false);
		//MultipleChoice_1a_Right.SetActive (false);

	}

	public void MC_5a_Wrong(){
		MultipleChoice_5b_Obj.SetActive (false);
		MultipleChoice_5a_Obj.SetActive (true);
		MultipleChoice_5a_Wrong.SetActive (false);

	}

	public void MC_5b_Wrong(){
		MultipleChoice_5b_Obj.SetActive (true);
		MultipleChoice_5a_Obj.SetActive (false);
		MultipleChoice_5b_Wrong.SetActive (false);

	}

	public void MC_5_Done(){
		MultipleChoice_5_Complete.SetActive (true);
	}


	public void check_MC_5a(Toggle theAnswer){
		if (theAnswer.isOn == true) {
			MultipleChoice_5a_Right.SetActive (true);
			MultipleChoice_5a_Wrong.SetActive (false);
			int plusScore = 0;
			plusScore = 10+ (int)(MC_5a_timer.getBonus());
			MC_5a_plus_text.text = "+ " + (plusScore.ToString ());
			updateScore (plusScore);
			updateFinished ("MC_5", 1);
			MC_5 = 1;
		} else {
			MultipleChoice_5a_Wrong.SetActive (true);
			MultipleChoice_5a_Right.SetActive (false);
		}
	}

	public void check_MC_5b(Toggle theAnswer){
		if (theAnswer.isOn == true) {
			MultipleChoice_5b_Right.SetActive (true);
			MultipleChoice_5b_Wrong.SetActive (false);
			int plusScore = 0;
			plusScore = 10+ (int)(MC_5b_timer.getBonus());
			MC_5b_plus_text.text = "+ " + (plusScore.ToString ());
			updateScore (plusScore);
			updateFinished ("MC_5", 2);
			MC_5 = 2;

		} else {
			MultipleChoice_5b_Wrong.SetActive (true);
			MultipleChoice_5b_Right.SetActive (false);

		}
	}











	public void correctAnswer_MC(GameObject nextQuestion){
		nextQuestion.SetActive (false);
		//MultipleChoice_1_Obj.SetActive (false);
		//AR_Camera_Obj.SetActive (true);
	}



	public void switchColor(int colorIndex){
		if (colorIndex <= (colorList.Count - 1)) {
			AvatarCircle.color = colorList [colorIndex];
			currColor = colorIndex;



			AvatarSpriteMap.color = colorList [currColor];
			AvatarSpriteMapInfo.color = colorList [currColor];
			AvatarCircle_Settings.color = colorList [currColor];
		} 

	}

	public void NextAvatar(){
		if (currAvatar < (Avatars.Count - 1)) {
			SetAvatar (currAvatar + 1);
		} else {
			SetAvatar (0);
		}
	}


	public void LastAvatar(){
		if (currAvatar > 0) {
			SetAvatar (currAvatar - 1);
		} else {
			SetAvatar ((Avatars.Count-1));
		}
	}

	public void SetAvatar(int avatarIndex){
		if ((Avatars.Count - 1) >= avatarIndex) {
			AvatarSprite.sprite = Avatars [avatarIndex];
			currAvatar = avatarIndex;

			AvatarSpriteMap.sprite = Avatars [avatarIndex];
			AvatarSpriteMapInfo.sprite = Avatars [avatarIndex];
			AvatarSpriteMapSettings.sprite = Avatars [avatarIndex];

		}

	}

	public void VideoCompleted (){
		vidCompletePop.SetActive (true);


	}




}
