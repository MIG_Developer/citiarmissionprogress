﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
using UnityEngine.Video;
using UnityEngine.UI;
public class ImageTargetPlayVideo : MonoBehaviour, ITrackableEventHandler
	{

	public UIManager UIMgr;

	public GameObject AR_Camera_Obj;

	public VideoPlayer vidPlayer;
	public string currVideoKey;

	public bool isDone;

		private TrackableBehaviour mTrackableBehaviour;

		void Start()
		{
		isDone = false;
		UIMgr.vidCompletePop.SetActive (false);

			mTrackableBehaviour = GetComponent<TrackableBehaviour>();
			if (mTrackableBehaviour)
			{
				mTrackableBehaviour.RegisterTrackableEventHandler(this);
			}

		vidPlayer.loopPointReached += OnEnd;
		}

	void OnDestroy(){
		vidPlayer.loopPointReached -= OnEnd;
	}

	void OnEnd(UnityEngine.Video.VideoPlayer vp){
		isDone = false;
		UIMgr.vidCompletePop.SetActive (true);
		UIMgr.updateScore (10);

	}

		public void OnTrackableStateChanged(
			TrackableBehaviour.Status previousStatus,
			TrackableBehaviour.Status newStatus)


		{



		if (UIMgr.ARmode == false) {
			
			if (vidPlayer.isPlaying == true) {
				vidPlayer.Pause ();
			}
			return;

		}


		if (newStatus == TrackableBehaviour.Status.DETECTED ||
				newStatus == TrackableBehaviour.Status.TRACKED ||
				newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
			{

			// Play audio when target is found
			if (isDone == false) {
	
				vidPlayer.Play ();
				AR_Camera_Obj.SetActive (true);
				Debug.Log ("found");
			} else {
				UIMgr.AR_Back_Button.SetActive (false);
				UIMgr.vidCompletePop.SetActive (true);
			}

			} else {
			if (isDone == false) {
				vidPlayer.Pause ();
				AR_Camera_Obj.SetActive (true);

				if (UIMgr.vidCompletePop.activeInHierarchy == true) {
					UIMgr.vidCompletePop.SetActive (false);
				}
				Debug.Log ("lost");
			} else {
				UIMgr.AR_Back_Button.SetActive (true);
				UIMgr.vidCompletePop.SetActive (false);
			}
			}
		}   


}
