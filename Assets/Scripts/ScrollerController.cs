﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EnhancedUI.EnhancedScroller;

public class ScrollerController : MonoBehaviour, IEnhancedScrollerDelegate {
	[HideInInspector]
	public List<ScrollerData> _data;

	public EnhancedScroller myScroller;
	public LeaderCellView leaderCellViewPrefab;

	// Use this for initialization
	public void StartScroller (List<ScrollerData> newScrollerData) {
		_data = newScrollerData;;


		myScroller.Delegate = this;
		myScroller.ReloadData ();

	}

	public int GetNumberOfCells(EnhancedScroller scroller){

		return _data.Count;
	}

	public float GetCellViewSize (EnhancedScroller scroller, int dataIndex) {
		return 100f;
	}

	public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex) {
		Debug.Log ("dataINdex="+dataIndex);
		Debug.Log("Datacount="+_data.Count);
		LeaderCellView cellView = scroller.GetCellView (leaderCellViewPrefab) as LeaderCellView;
		cellView.SetData ((_data [dataIndex]));
		return cellView;
	}

}
