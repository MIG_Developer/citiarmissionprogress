﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using Vuforia;

public class ImageTargetGoals : MonoBehaviour, ITrackableEventHandler {
	public GameObject UserInputPanel;
	public UIManager MainCanvas;

	public UnityEvent runScript;

	private TrackableBehaviour mTrackableBehaviour;
	private bool isTracked;

	void Start()

	{

		isTracked = false;

		if(runScript == null){
			runScript = new UnityEvent ();
		}


		mTrackableBehaviour = GetComponent<TrackableBehaviour>();
		if (mTrackableBehaviour)
		{
			mTrackableBehaviour.RegisterTrackableEventHandler(this);
		}
	}

	public void OnTrackableStateChanged(
		TrackableBehaviour.Status previousStatus,
		TrackableBehaviour.Status newStatus)
	{
		if (MainCanvas.ARmode == false) {
			isTracked = false;
			UserInputPanel.SetActive (false);
			MainCanvas.AR_Camera_Obj.SetActive (false);
			MainCanvas.AR_Back_Button.SetActive (true);
	
			return;
		}
		// 
		if (
			newStatus == TrackableBehaviour.Status.DETECTED ||
			newStatus == TrackableBehaviour.Status.TRACKED ||
			newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
		{



			// Play audio when target is found

			if (isTracked == false)
			{				
				
				runScript.Invoke ();
				isTracked = true;
				UserInputPanel.SetActive (true);
				Debug.Log ("found");
			}

		} else {
			if (isTracked == true) {
				isTracked = false;

				MainCanvas.AR_Camera_Obj.SetActive (true);
				MainCanvas.AR_Back_Button.SetActive (true);
				UserInputPanel.SetActive (false);
				Debug.Log ("lostIMG");
			}

		}

	}
}
