﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeleteUserAccount : MonoBehaviour {
	public GameObject DeletePanel;
	public UIManager UIMgr;
	// Use this for initialization
	void Start () {
		DeletePanel.SetActive(false);
	}
	
	public void ShowLogUserOutWarning(){
		DeletePanel.SetActive(true);
	}

	public void cancelLogUserOut(){

		DeletePanel.SetActive(false);
	}

	public void LogUserOutOK(){

		DeletePanel.SetActive(false);
		UIMgr.LogOutOfFireBase();
	}
}
