﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class setVideo : MonoBehaviour {
	public VideoPlayer vidPlayer;
	public string videoFile;
	// Use this for initialization
	void Start () {
		vidPlayer.url = "jar:file://" + Application.dataPath + "!/assets/"+videoFile;
		vidPlayer.playOnAwake = false;



		vidPlayer.Pause ();
	}

}
